#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Coil combination method using SVD and ESPIRiT.

Note that there are a few software pre-requisites:
- BART [https://mrirecon.github.io/bart/]
- `nibabel` Python package
- `numpy` Python package
- `blessed` or `blessings` Python package (optional)

Please modify the FIXME items according to your setup!

References:
    Berkin, B. et al. Proc.ISMRM, #2849 (2016)
    Uecker, M. et al. Magn. Reson. Med. 71, 990–1001 (2014)
"""

#    Copyright (C) 2016 Riccardo Metere <metere@cbs.mpg.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ======================================================================
# :: Future Imports
from __future__ import (
    division, absolute_import, print_function, unicode_literals)

# ======================================================================
# :: Python Standard Library Imports
import os  # Miscellaneous operating system interfaces
import sys  # System-specific parameters and functions
import datetime  # Basic date and time types
import argparse  # Parser for command-line options, arguments and subcommands
import itertools  # Functions creating iterators for efficient looping
import subprocess  # Subprocess management
import shlex  # Simple lexical analysis

# :: External Imports
import numpy as np  # NumPy (multidimensional numerical arrays library)
import nibabel as nib  # NiBabel (NeuroImaging I/O Library)

# :: External Imports Submodules


# ======================================================================
# :: Versioning
__version__ = '1.0.0'

# ======================================================================
# :: Project Details
INFO = {
    'authors': (
        'Riccardo Metere <metere@cbs.mpg.de>',
        'Ahmad Kanaan <kanaan@cbs.mpg.de',
    ),
    'copyright': 'Copyright (C) 2015',
    'license': 'License: GNU General Public License version 3 (GPLv3)',
    'notice':
        """
This program is free software and it comes with ABSOLUTELY NO WARRANTY.
It is covered by the GNU General Public License version 3 (GPLv3).
You are welcome to redistribute it under its terms and conditions.
        """,
    'version': __version__
}

# ======================================================================
# :: supported verbosity levels (level 4 skipped on purpose)
VERB_LVL_NAMES = (
    'none', 'lowest', 'lower', 'low', 'medium', 'high', 'higher', 'highest',
    'warning', 'debug')
VERB_LVL = {k: v for k, v in zip(VERB_LVL_NAMES, range(len(VERB_LVL_NAMES)))}
D_VERB_LVL = VERB_LVL['lowest']

# ======================================================================
BART_CMD_PATH = os.path.expanduser('~/.local/bin/')
BART_CMD = ('fft', 'ecalib', 'slice')

CMD = {}
for name in BART_CMD:
    CMD[name] = os.path.join(BART_CMD_PATH, 'bart {name}'.format(name=name))
CMD['gzip'] = '/bin/gzip'


# ======================================================================
# -- BEGIN DERIVED CODE

# see: https://github.com/mrirecon/bart/blob/master/python/cfl.py

# Copyright 2013-2015. The Regents of the University of California.
# All rights reserved. Use of this source code is governed by
# a BSD-style license which can be found in the LICENSE file.
#
# Authors:
# 2013 Martin Uecker <uecker@eecs.berkeley.edu>
# 2015 Jonathan Tamir <jtamir@eecs.berkeley.edu>


def readcfl(name):
    # get dims from .hdr
    h = open(name + ".hdr", "r")
    h.readline()  # skip
    l = h.readline()
    h.close()
    dims = [int(i) for i in l.split()]

    # remove singleton dimensions from the end
    n = np.prod(dims)
    dims_prod = np.cumprod(dims)
    dims = dims[:np.searchsorted(dims_prod, n) + 1]

    # load data and reshape into dims
    d = open(name + ".cfl", "r")
    a = np.fromfile(d, dtype=np.complex64, count=n)
    d.close()
    return a.reshape(dims, order='F')  # column-major


def writecfl(array, name):
    h = open(name + ".hdr", "w")
    h.write('# Dimensions\n')
    for i in (array.shape):
        h.write("%d " % i)
    h.write('\n')
    h.close()
    d = open(name + ".cfl", "w")
    array.T.astype(np.complex64).tofile(d)  # tranpose for column-major order
    d.close()


# -- END DERIVED CODE
# ======================================================================


# ======================================================================
def execute(
        cmd,
        in_pipe=None,
        get_out_pipes=True,
        dry=False,
        verbose=D_VERB_LVL):
    """
    Execute command and retrieve/print output at the end of execution.

    Args:
        cmd (str): Command to execute.
        in_pipe (str): Input data to be used as stdin of the process.
        get_out_pipes (bool): Get stdout and stderr streams from the process.
            If True, the program flow is halted until the process is completed.
            Otherwise, the process is spawn in background, continuing execution.
        dry (bool): Print rather than execute the command (dry run).
        verbose (int): Set level of verbosity.

    Returns:
        ret_code (int): if get_pipes, the return code of the command.
        p_stdout (str|None): if get_pipes, the stdout of the process.
        p_stderr (str|None): if get_pipes, the stderr of the process.
    """
    ret_code, p_stdout, p_stderr = None, None, None
    # ensure cmd is a list of strings
    try:
        cmd = shlex.split(cmd)
    except AttributeError:
        pass

    if dry:
        msg('$$ {}'.format(' '.join(cmd)))
    else:
        msg('>> {}'.format(' '.join(cmd)), verbose, VERB_LVL['higher'])

        proc = subprocess.Popen(
            cmd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=False, close_fds=True, universal_newlines=True)

        if in_pipe is not None:
            msg('< {}'.format(in_pipe), verbose, VERB_LVL['higher'])
            proc.stdin.write(in_pipe)
            proc.stdin.detach()

        if get_out_pipes:
            # handle stdout
            p_stdout = ''
            while proc.poll() is None:
                stdout_buffer = proc.stdout.readline()
                p_stdout += stdout_buffer
                if verbose >= VERB_LVL['highest']:
                    print(stdout_buffer, end='')
                    sys.stdout.flush()
            # handle stderr
            p_stderr = proc.stderr.read()
            if verbose >= VERB_LVL['high']:
                print(p_stderr)
            # finally get the return code
            ret_code = int(proc.returncode)

    return ret_code, p_stdout, p_stderr


# ======================================================================
def msg(
        text,
        verb_lvl=D_VERB_LVL,
        verb_threshold=D_VERB_LVL,
        fmt=None,
        *args,
        **kwargs):
    """
    Display a feedback message to the standard output.

    Args:
        text (str|Any): Message to display or object with `__repr__`.
        verb_lvl (int): Current level of verbosity.
        verb_threshold (int): Threshold level of verbosity.
        fmt (str): Format of the message (if `blessed` supported).
            If None, a standard formatting is used.
        *args (tuple): Positional arguments to be passed to `print`.
        **kwargs (dict): Keyword arguments to be passed to `print`.

    Returns:
        None.

    Examples:
        >>> s = 'Hello World!'
        >>> msg(s)
        Hello World!
        >>> msg(s, VERB_LVL['medium'], VERB_LVL['low'])
        Hello World!
        >>> msg(s, VERB_LVL['low'], VERB_LVL['medium'])  # no output
        >>> msg(s, fmt='{t.green}')  # if ANSI Terminal, green text
        Hello World!
        >>> msg('   :  a b c', fmt='{t.red}{}')  # if ANSI Terminal, red text
           :  a b c
        >>> msg(' : a b c', fmt='cyan')  # if ANSI Terminal, cyan text
         : a b c
    """
    if verb_lvl >= verb_threshold and text:
        # if blessed is not present, no coloring
        try:
            import blessed
        except ImportError:
            try:
                import blessings as blessed
            except ImportError:
                blessed = None

        if blessed:
            text = str(text)
            t = blessed.Terminal()
            if not fmt:
                if VERB_LVL['low'] < verb_threshold <= VERB_LVL['medium']:
                    e = t.cyan
                elif VERB_LVL['medium'] < verb_threshold < VERB_LVL['debug']:
                    e = t.magenta
                elif verb_threshold >= VERB_LVL['debug']:
                    e = t.blue
                elif text.startswith('I:'):
                    e = t.green
                elif text.startswith('W:'):
                    e = t.yellow
                elif text.startswith('E:'):
                    e = t.red
                else:
                    e = t.white
                # first non-whitespace word
                txt1 = text.split(None, 1)[0]
                # initial whitespaces
                n = text.find(txt1)
                txt0 = text[:n]
                # rest
                txt2 = text[n + len(txt1):]
                txt_kws = dict(
                    e1=e + (t.bold if e == t.white else ''),
                    e2=e + (t.bold if e != t.white else ''),
                    t0=txt0, t1=txt1, t2=txt2, n=t.normal)
                text = '{t0}{e1}{t1}{n}{e2}{t2}{n}'.format_map(txt_kws)
            else:
                if 't.' not in fmt:
                    fmt = '{{t.{}}}'.format(fmt)
                if '{}' not in fmt:
                    fmt += '{}'
                text = fmt.format(text, t=t) + t.normal
        print(text, *args, **kwargs)


# ======================================================================
def check_redo(
        in_filepaths,
        out_filepaths,
        force=False):
    """
    Check if input files are newer than output files, to force calculation.

    Args:
        in_filepaths (list[str]): Input filepaths for computation.
        out_filepaths (list[str]): Output filepaths for computation.
        force (bool): Force computation to be re-done.

    Returns:
        force (bool): True if the computation is to be re-done.

    Raises:
        IndexError: If the input filepath list is empty.
        IOError: If any of the input files do not exist.
    """
    # todo: include output_dir autocreation
    # check if input is not empty
    if not in_filepaths:
        raise IndexError('List of input files is empty.')

    # check if input exists
    for in_filepath in in_filepaths:
        if not os.path.exists(in_filepath):
            raise IOError('Input file does not exists.')

    # check if output exists
    if not force:
        for out_filepath in out_filepaths:
            if out_filepath:
                if not os.path.exists(out_filepath):
                    force = True
                    break

    # check if input is older than output
    if not force:
        for in_filepath, out_filepath in \
                itertools.product(in_filepaths, out_filepaths):
            if in_filepath and out_filepath:
                if os.path.getmtime(in_filepath) \
                        > os.path.getmtime(out_filepath):
                    force = True
                    break
    return force


# ======================================================================
def combine_names(first, second):
    items = []
    for s in (first, second):
        offset = 2 if s.endswith('gz') else 1
        items.append('.'.join(os.path.basename(s).split('.')[:-offset]))
    return '__'.join(items)


# ======================================================================
def scale(
        val,
        out_interval=None,
        in_interval=None):
    """
    Linear convert the value from input interval to output interval

    Args:
        val (float|np.ndarray): Value(s) to convert.
        out_interval (float,float): Interval of the output value.
            If None, set to: (0, 1).
        in_interval (float,float): Interval of the input value.
            If None, and val is iterable, it is calculated as:
            (min(val), max(val)), otherwise set to: (0, 1).

    Returns:
        val (float): The converted value

    Examples:
        >>> scale(100, (0, 1000), (0, 100))
        1000.0
        >>> scale(50, (0, 1000), (-100, 100))
        750.0
        >>> scale(50, (0, 10), (0, 1))
        500.0
        >>> scale(0.5, (-10, 10))
        0.0
        >>> scale(np.pi / 3, (0, 180), (0, np.pi))
        60.0
        >>> scale(np.arange(5), (0, 1))
        array([ 0.  ,  0.25,  0.5 ,  0.75,  1.  ])
        >>> scale(np.arange(6), (0, 10))
        array([  0.,   2.,   4.,   6.,   8.,  10.])
        >>> scale(np.arange(6), (0, 10), (0, 2))
        array([  0.,   5.,  10.,  15.,  20.,  25.])
    """
    if in_interval:
        in_min, in_max = sorted(in_interval)
    elif isinstance(val, np.ndarray):
        in_min, in_max = np.min(val), np.max(val)
    else:
        in_min, in_max = (0, 1)
    if out_interval:
        out_min, out_max = sorted(out_interval)
    else:
        out_min, out_max = (0, 1)
    return (val - in_min) / (in_max - in_min) * (out_max - out_min) + out_min


# ======================================================================
def minmax(arr):
    """
    Calculate the minimum and maximum of an array: (min, max).

    Args:
        arr (np.ndarray): The input array.

    Returns:
        min (float): the minimum value of the array
        max (float): the maximum value of the array

    Examples:
        >>> minmax(np.arange(10))
        (0, 9)
    """
    return np.min(arr), np.max(arr)


# ======================================================================
def fix_phase_interval(arr):
    """
    Ensure that the range of values is interpreted as valid phase information.

    This is useful for DICOM-converted images (without post-processing).

    Args:
        arr (np.ndarray): Array to be processed.

    Returns:
        array (np.ndarray): An array scaled to (-pi,pi).
    """
    # correct phase value range (useful for DICOM-converted images)
    if np.ptp(arr) > 2.0 * np.pi:
        arr = scale(arr, (-np.pi, np.pi))
    return arr


# ======================================================================
def combine_coils_espirit_svd(
        in_mag_filepath,
        in_phs_filepath,
        out_mag_filepath,
        out_phs_filepath,
        svd_threshold=None,
        calib_region=16,
        keep=False,
        force=False,
        verbose=D_VERB_LVL):
    """
    Coil combination using ESPIRiT and SVD to obtain the virtual reference coil

    args:
        in_mag_filepath (str): Input magnitude filepath with individual coil
            images in the last dimension
        in_phs_filepath (str): Input phase filepath with individual coil
            images in the last dimension
        out_mag_filepath (str): Output magnitude filepath
        out_phs_filepath (str): Output phase filepath
        svd_threshold (float): Determines the number of SVD components to use:
            num_svd = int(num_coils * threshold)
            If None, uses the number of components larger than the average.
        calib_region (int): Size of the calibration region for BART ESPIRiT.
            Please refer to BART for further documentation.
        keep (bool): Keep cached temporary files.
        force (bool): Force new processing
        verbose (int): Set the level of verbosity

    Returns:
        None
    """
    msg('Inspect input `{}`, `{}`'.format(in_mag_filepath, in_phs_filepath),
        verbose, VERB_LVL['medium'])

    cached = []

    tmp = combine_names(in_mag_filepath, in_phs_filepath)

    # load images
    nii_mag = nib.load(in_mag_filepath)
    nii_phs = nib.load(in_phs_filepath)

    img_coils_mag = nii_mag.get_data()
    img_coils_phs = fix_phase_interval(nii_phs.get_data())

    img_coils = img_coils_mag * np.exp(1j * img_coils_phs)
    num_coils = img_coils.shape[-1]

    if not svd_threshold:
        svd_threshold = 0.5
    num_svd = int(num_coils * min(max(svd_threshold, 0.1), 1.0))

    svd_filepath = '{}__svd_{}.cfl'.format(tmp, num_svd)
    cached.append(svd_filepath)
    if check_redo([in_mag_filepath, in_phs_filepath], [svd_filepath], force):
        # perform SVD
        arr_coils = img_coils.reshape(-1, num_coils)
        eigvals, eigvects = np.linalg.eig(
            np.dot(arr_coils.conj().T, arr_coils))

        # coil compressed images, where 1st chan is the virtual body coil
        img_svd = np.dot(
            arr_coils, eigvects[:, :num_svd]).reshape(
            img_coils.shape[0:-1] + (num_svd,))
        out_file = os.path.splitext(svd_filepath)[0]
        msg('Writing image: {}'.format(svd_filepath),
            verbose, VERB_LVL['medium'])
        writecfl(img_svd, out_file)
    else:
        in_file = os.path.splitext(svd_filepath)[0]
        img_svd = readcfl(in_file)

    # getting ESPIRiT calibration done in BART
    msg('Performing BART ESPIRiT computation...', verbose, VERB_LVL['medium'])
    # dim_bitmask: 7 = 0b111 for all spatial dimensions
    svdk_filepath = '{}__svd_k_{}.cfl'.format(tmp, num_svd)
    cached.append(svdk_filepath)
    if check_redo([svd_filepath], [svdk_filepath], force):
        in_file = os.path.splitext(svd_filepath)[0]
        out_file = os.path.splitext(svdk_filepath)[0]
        execute(
            CMD['fft'] + ' {dim_bitmask} {in_file} {out_file}'.format(
                dim_bitmask=7, in_file=in_file, out_file=out_file),
            verbose=verbose)

    svdc_filepath = '{}__calib_{}_{}.cfl'.format(tmp, num_svd, calib_region)
    cached.append(svdc_filepath)
    if check_redo([svdk_filepath], [svdc_filepath], force):
        in_file = os.path.splitext(svdk_filepath)[0]
        out_file = os.path.splitext(svdc_filepath)[0]
        execute(
            CMD['ecalib'] + ' -r {c} {in_file} {out_file}'.format(
                c=calib_region, in_file=in_file, out_file=out_file),
            verbose=verbose)

    svds_filepath = '{}__svd_sens_{}_{}.cfl'.format(tmp, num_svd, calib_region)
    cached.append(svds_filepath)
    if check_redo([svdc_filepath], [svds_filepath], force):
        in_file = os.path.splitext(svdc_filepath)[0]
        out_file = os.path.splitext(svds_filepath)[0]
        execute(
            CMD['slice'] + ' {dim} {pos} {in_file} {out_file}'.format(
                dim=4, pos=0, in_file=in_file, out_file=out_file),
            verbose=verbose)

    # read estimated coil sensitivities and combine them
    if check_redo([svds_filepath], [out_mag_filepath, out_phs_filepath], force):
        in_file = os.path.splitext(svds_filepath)[0]
        sensitivity = readcfl(in_file)

        msg('Combining coils together...', verbose, VERB_LVL['medium'])
        img_combined = np.sum(img_svd * sensitivity.conj(), -1) / (
            np.finfo(np.float).eps +
            np.sum(np.abs(sensitivity) * np.abs(sensitivity), -1))

        msg('Saving `{}`, `{}`'.format(out_mag_filepath, out_phs_filepath),
            verbose, VERB_LVL['medium'])
        nib.Nifti1Image(
            np.abs(img_combined), nii_mag.get_affine()).to_filename(
            out_mag_filepath)
        nib.Nifti1Image(
            np.angle(img_combined), nii_phs.get_affine()).to_filename(
            out_phs_filepath)

    if not keep:
        for filepath in cached:
            base, ext = os.path.splitext(filepath)
            if ext == '.cfl':
                os.remove(filepath)
                os.remove(base + '.hdr')


# ======================================================================
def handle_arg():
    """
    Handle command-line application arguments.
    """
    # :: Create Argument Parser
    arg_parser = argparse.ArgumentParser(
        description=__doc__,
        epilog='v.{} - {}\n{}'.format(
            INFO['version'], ', '.join(INFO['authors']),
            INFO['license']),
        formatter_class=argparse.RawDescriptionHelpFormatter)
    # :: Add POSIX standard arguments
    arg_parser.add_argument(
        '--ver', '--version',
        version='%(prog)s - ver. {}\n{}\n{} {}\n{}'.format(
            INFO['version'],
            next(line for line in __doc__.splitlines() if line),
            INFO['copyright'], ', '.join(INFO['authors']),
            INFO['notice']),
        action='version')
    arg_parser.add_argument(
        '-v', '--verbose',
        action='count', default=D_VERB_LVL,
        help='increase the level of verbosity [%(default)s]')
    arg_parser.add_argument(
        '-q', '--quiet',
        action='store_true',
        help='override verbosity settings to suppress output [%(default)s]')
    # :: Add additional arguments
    arg_parser.add_argument(
        '-f', '--force',
        action='store_true',
        help='force new processing [%(default)s]')
    arg_parser.add_argument(
        '-x', '--keep',
        action='store_true',
        help='keep cached temporary files [%(default)s]')
    arg_parser.add_argument(
        '-m', '--in_mag_filepath', metavar='FILE',
        default='mag_coils.nii.gz',
        help='set magnitude input filepath [%(default)s]')
    arg_parser.add_argument(
        '-p', '--in_phs_filepath', metavar='FILE',
        default='phs_coils.nii.gz',
        help='set phase input filepath [%(default)s]')
    arg_parser.add_argument(
        '-l', '--out_mag_filepath', metavar='FILE',
        default='mag.nii.gz',
        help='set magnitude output filepath [%(default)s]')
    arg_parser.add_argument(
        '-o', '--out_phs_filepath', metavar='FILE',
        default='phs.nii.gz',
        help='set phase output filepath [%(default)s]')
    arg_parser.add_argument(
        '-k', '--svd_threshold', metavar='X',
        type=float,
        default=None,
        help='set the SVD threshold k (k in [0.1, 1]). N_svd = N_coil * k.'
             '[%(default)s]')
    arg_parser.add_argument(
        '-c', '--calib_region', metavar='N',
        type=int,
        default=16,
        help='size of the calibration region for BART ESPIRiT [%(default)s]')
    return arg_parser


# ======================================================================
def main():
    # :: handle program parameters
    arg_parser = handle_arg()
    args = arg_parser.parse_args()
    # fix verbosity in case of 'quiet'
    if args.quiet:
        args.verbose = VERB_LVL['none']
    # :: print debug info
    if args.verbose >= VERB_LVL['debug']:
        arg_parser.print_help()
    msg('\nARGS: ' + str(vars(args)), args.verbose, VERB_LVL['debug'])
    msg(__doc__.strip(), args.verbose, VERB_LVL['lower'])

    begin_time = datetime.datetime.now()

    kws = vars(args)
    kws.pop('quiet')
    combine_coils_espirit_svd(**kws)

    end_time = datetime.datetime.now()
    if args.verbose > VERB_LVL['low']:
        print('ExecTime: {}'.format(end_time - begin_time))


# ======================================================================
if __name__ == '__main__':
    main()
