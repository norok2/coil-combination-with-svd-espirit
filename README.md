# Coil combination method using SVD and ESPIRiT #

This script can be used as a command-line tool to combine individual coil data into a single image using the SVD-ESPIRiT coil combination technique.


## Requirements ##

The following software should be installed prior to using the script:

- Python 2.7 or Python 3.x (tested on Python 3.5)
- BART [https://mrirecon.github.io/bart/]
- `nibabel` Python package
- `numpy` Python package
- `blessed` or `blessings` Python package (optional)


## Installation ##

- latest / development versions of the scripts can be found at: [https://bitbucket.org/norok2/coil-combination-with-svd-espirit]
- download the [script](coil_combination_svd_espirit.py); for your convenience it should be placed inside a directory where executables are usually located, e.g. `~/.local/bin`.
- ensure that the `bart` executable is in the directory specified by `BART_CMD_PATH` in the scrip; by default this is `~/.local/bin`. To do so, please use any of the following approaches:
    - change `BART_CMD_PATH` to the correct value; if `bart` is available system-wide, the correct value can be obtained by ``dirname `command -v bart` ``
    - copy the `bart` executable to the path specified in `BART_CMD_PATH`
- (optional) make sure that the directory is in your `PATH`
- (optional) make sure that the script is executable, e.g. with
```bash
chmod a+x coil_combination_svd_espirit.py
```
- run the script! :-) (see **Usage** for details).

## Usage ##

The script requires two NIfTI images (magnitude and phase) containing 4D volumes where the first three dimensions are for spatial information and the individual channels are through the fourth dimension.

Depending on your setup, the script runs with: 
```bash
python coil_combination_svd_espirit.py
```
or:
```bash
coil_combination_svd_espirit.py
```
or:
```bash
./coil_combination_svd_espirit.py
```

Additional explanation, including for the accepted arguments, will be output by issuing:
```bash
coil_combination_svd_espirit.py --help
```
(or equivalent).


## Copyright ##

    Copyright (C) 2016 Riccardo Metere <metere@cbs.mpg.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Acknowledgements ##

This script is loosely based on: [http://martinos.org/~berkin/SVD_ESPIRiT_Coil_Combine_Toolbox_v2.zip]


## References ##

1. Metere, R., Kanaan, A.S., Bilgic, B., Schlumm, T., Moeller, H.E., 2017. Effects of the coil combination algorithm on quantitative susceptibility mapping. Under Review.
2. Chatnuntawech, I., McDaniel, P., Cauley, S.F., Gagoski, B.A., Langkammer, C., Martin, A., Grant, P.E., Wald, L.L., Setsompop, K., Adalsteinsson, E., Bilgic, B., 2016. Single-step quantitative susceptibility mapping with variational penalties. NMR Biomed. n/a-n/a. doi:10.1002/nbm.3570
3. Bilgic, B., Polimeni, J.R., Wald, L.L., Setsompop, K., 2016. Automated tissue phase and QSM estimation from multichannel data, in: Proceedings of the ISMRM 24th Annual Meeting & Exhibition. Presented at the 24th Annual Meeting & Exhibition of the International Society for Magnetic Resonance in Medicine, Singapore.
4. Uecker, M., Lustig, M., 2016. Estimating absolute-phase maps using ESPIRiT and virtual conjugate coils. Magn. Reson. Med. n/a-n/a. doi:10.1002/mrm.26191